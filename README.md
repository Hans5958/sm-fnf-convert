# Converted FNF Simfiles

Some of my converted FNF simfiles that I made using the tool I forked. Obviously, for Stepmania usage.

All songs, charts, and anything in between are made by their respective authors. I'm just here converting it.

## Notes 

- I tried my best to set the proper authors on the simfiles, but there are some that are just too confusing to be put there. Either way, I linked the mod pages in this README.
- The Challenge chart is a merge between two sides of the Hard difficulty, based on respective `mustHitSection` values on their parts. (The key is quite misleading, it just controls the camera.)
- The difficulty number on these are not rated correctly! Use other tools like Etterna to figure out the real difficulty (or do it yourself).

## Index

- `GRn`: **Smoke 'Em Out Struggle** (VS Garcello)
  https://gamebanana.com/mods/166531
- `HMn`: Friday Night Funkin' **+ Hatsune Miku**  
  https://gamebanana.com/mods/44307
- `HXwn`: **VS Hex**  
  https://gamebanana.com/mods/44225
- `KPwn`: **VS. Kapi - Arcade Showdown**  
  https://gamebanana.com/mods/44683
- `SVn`: **Sarvente's Mid-Fight Masses**  
  https://gamebanana.com/mods/44345 
- `TBn`: **V.S. Tabi** Ex Boyfriend  
  https://gamebanana.com/mods/286388
- `TRn`: The **Tricky Mod**  
  https://gamebanana.com/mods/44334
- `WFwn`: **Wii Funkin'** (Vs Matt)  
https://gamebanana.com/mods/44511
- `WTnv`: **VS Whitty**  
  https://gamebanana.com/mods/44214
- `XEwn`: **The X Event**  
  https://gamebanana.com/mods/44385
- `ZRn`: **V.S Zardy - Foolhardy**  
  https://gamebanana.com/mods/44366

### Key legend:

- `n`: Order of the song in a week
- `v`: Variations of the song (0 is original, others are variations)
- `w`: The week of the song (Songs that are not included in a certain week are also separated)

### Example

```
XE10 Overwrite
│ ││ │
│ ││ └─ The name of the song shown in FNF (usually shortened)
│ │└─── Variation of the song (0, which means the original)
│ └──── Order of the song in the week (number 1)
└────── ID of the mod in this repository (The X Event)
```